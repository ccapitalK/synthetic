#pragma once
#include<cstdint>
#include<string>
#include<fstream>

template <typename T> constexpr T idToNum(const char * id){
    static_assert(sizeof(T)==sizeof(uint32_t));
    uint32_t retVal=0;
    for(int i = 3; i >= 0; --i){
        retVal<<=8;
        retVal+=(uint32_t)id[i];
    }
    return static_cast<T>(retVal);
}

template <typename T> T getID(std::ifstream & ifile){
    char buf[sizeof(T)];
    ifile.read(buf, sizeof(T));
    return idToNum<T>(buf);
}

template <typename T> std::string numToID(T ID){
    static_assert(sizeof(T)==sizeof(uint32_t));
    auto id = static_cast<uint32_t>(ID);
    std::string retString;
    for(int i = 0; i < 4; ++i){
        uint8_t buf = (id>>(8*i))&0xFF;
        retString.push_back(buf);
    }
    return retString;
}

#define idEnt(x) x=idToNum<uint32_t>(#x)
enum class ckID : uint32_t {
    NONE=0,
    idEnt(RIFF),
    idEnt(INFO),
    idEnt(LIST),
    idEnt(ifil),
};

enum class fccType : uint32_t {
    NONE=0,
    idEnt(sfbk),
};

enum class midiTAG : uint32_t {
    NONE=0,
    idEnt(MThd),
    idEnt(MTrk),
};
#undef idEnt


class soundfont {
    std::ifstream ifile;
    std::string fileName;
public:
    soundfont(std::string file_name);
    void open();
};
