#include"synth.h"
#include"log.h"

void synthesizer::load(std::string fileName){
    logger::miParserLog << "Opening \"" << fileName << "\"" << std::endl;
    hasHeader=false;
    ifile.open(fileName, std::ios::in | std::ios::binary);
    if(ifile.fail()){
        throw std::runtime_error("Failed to open file");
    }
    while(!ifile.eof()){
        auto currentChunkType=getID<midiTAG>(ifile);
        auto chunkSize=getUBE<uint32_t>();
        switch(currentChunkType){
            case midiTAG::MThd:
                addThd(chunkSize);
                break;
            case midiTAG::MTrk:
                addTrk(chunkSize);
                break;
            default:
                logger::miParserLog << "Encountered alien tag: \"" << numToID<midiTAG>(currentChunkType) << "\"" << std::endl;
                ifile.ignore(chunkSize);
                break;
        }
    }
    ifile.close();
}

uint32_t synthesizer::getVLQ(){
    uint32_t retVal=0;
    char buf;
    ifile.read(&buf, 1);
    while(buf&0x80){
        retVal+=buf&0x7F;
        retVal<<=7;
        ifile.read(&buf, 1);
    }
    retVal+=buf&0x7F;
    return retVal;
}

template <typename T, size_t size> T synthesizer::getUBE(){
    const size_t sizeBytes=size/8;
    char buf[sizeBytes];
    ifile.read(buf, sizeBytes);
    if(ifile.fail()){
        unexpectedEOF();
    }
    T retVal=0;
    for(auto &i : buf){
        retVal<<=8;
        retVal+=static_cast<uint8_t>(i);
    }
    return retVal;
}

template <typename T, size_t size> T synthesizer::getULE(){
    const size_t sizeBytes=size/8;
    char buf[sizeBytes];
    ifile.read(buf, sizeBytes);
    if(ifile.fail()){
        unexpectedEOF();
    }
    T retVal=0;
    for(auto i = 0u; i < sizeBytes; ++i){
        retVal+=((uint32_t)reinterpret_cast<uint8_t>(buf[i]))<<(8*i);
    }
    return retVal;
}

void synthesizer::unexpectedEOF(){
    throw std::runtime_error("Premature EOF in input file");
}

void synthesizer::addThd(uint32_t size){
    if(hasHeader){
        throw std::runtime_error("File has multiple headers");
    }
    if(size!=6){
        throw std::runtime_error("File has header of incorrect length");
    }
    format    = getUBE<uint16_t>();
    if(format!=0){
        throw std::runtime_error("Only single track MIDIs are currently supported");
    }
    nTrks     = getUBE<uint16_t>();
    tracks.reserve(nTrks);
    auto div  = getUBE<uint16_t>();
    useSMPTE  = div&0x8000;
    if(!useSMPTE){
        ticksPerQuaterNote = div&0x7FFF;
    }else{
        throw std::runtime_error("SMPTE based MIDI timing is unsupported");
    }
    logger::miParserLog << "MIDI Header:\n\tformat = " << format << "\n\tnTrks = " << nTrks << "\n\tticksPerQuaterNote = " << ticksPerQuaterNote << std::endl;
    hasHeader = true;
}

void synthesizer::_parser_getSeqName(int trkNum){
    const u32 length = getVLQ();
    std::string text(length, ' ');
    ifile.read(&text[0], length);
    logger::miParserLog << "Sequence Name: " << text << std::endl;
}

void synthesizer::_parser_getCopyRight(int trkNum){
    const u32 length = getVLQ();
    std::string text(length, ' ');
    ifile.read(&text[0], length);
    logger::miParserLog << "Copyright Info: " << text << std::endl;
}

void synthesizer::_parser_getTextEvent(int trkNum){
    const u32 length = getVLQ();
    std::string text(length, ' ');
    ifile.read(&text[0], length);
    logger::miParserLog << "Text Event: " << text << std::endl;
}

void synthesizer::_parser_getTextMarker(int trkNum){
    const u32 length = getVLQ();
    std::string text(length, ' ');
    ifile.read(&text[0], length);
    logger::miParserLog << "Text Marker: " << text << std::endl;
}

void synthesizer::_parser_setTimeSignature(){ //TODO: Make this code do something
    const u8 numerator   = getUBE<u8>();
    const u8 denumerator = getUBE<u8>();
    const u8 clicks      = getUBE<u8>();
    const u8 bb          = getUBE<u8>(); //TODO: figure out what this does
    logger::miParserLog << "TODO: Set tempo(nn=" << (u32)numerator 
        << ", dd=" << (u32)denumerator 
        << ", cc=" << (u32)clicks 
        << ", bb=" << (u32)bb << ")" << std::endl;
}

void synthesizer::_parser_setKeySignature(){ //TODO: Make this code do something
    const u8 sf          = getUBE<u8>();
    const u8 mi          = getUBE<u8>();
    logger::miParserLog << "TODO: Set Key Signature(sf=" << (u32)sf 
        << ", mi=" << (u32)mi << ")" << std::endl;
}

void synthesizer::_parser_setTempo(){
    logger::miParserLog << "Current Offset: " << ifile.tellg() << std::endl;
    const u32 tempo = getUBE<u32, 24>();
    logger::miParserLog << "TODO: Set Key Signature(tempo=" << (u32)tempo << ")" << std::endl;
}

void synthesizer::_parser_programChange(u8 channelNo, u64 cumulativeDelay){
    //TODO: Make this code do something
    const u8 progNo = getUBE<u8>()&0x7f;
    logger::miParserLog << "MIDI[" << (u32)channelNo
        << "]: Program Change to " << (u32)progNo
        << " at time " << cumulativeDelay << std::endl;
}

void synthesizer::_parser_controlChange(u8 channelNo, u64 cumulativeDelay){
    //TODO: Make this code do something
    const u8 contNo = getUBE<u8>()&0x7f;
    const u8 value  = getUBE<u8>()&0x7f;
    logger::miParserLog << "MIDI[" << (u32)channelNo
        << "]: Control Change of controller " << (u32)contNo
        << " to " << (u32)value
        << " at time " << cumulativeDelay << std::endl;
}

void synthesizer::parseMetaEvent(std::string & prefix, int trkNum, u64 cumulativeDelay){
    u8 code = getUBE<u8>();
    prefix.push_back(code);
    switch(code){
        case 0x01:
            _parser_getTextEvent(trkNum);
            break;//arbitrary text event
        case 0x02:
            _parser_getCopyRight(trkNum);
            break;
        case 0x03:
            _parser_getSeqName(trkNum);
            break;
        case 0x06:
            _parser_getTextMarker(trkNum);
            break;
        case 0x51:
            prefix.push_back(getUBE<u8>());
            if(u32(prefix.back())!=0x03)
                throw prefix;
            _parser_setTempo();
            break;
        case 0x58:
            prefix.push_back(getUBE<u8>());
            if(u32(prefix.back())!=0x04)
                throw prefix;
            _parser_setTimeSignature();
            break;
        case 0x59:
            prefix.push_back(getUBE<u8>());
            if(u32(prefix.back())!=0x02)
                throw prefix;
            _parser_setKeySignature();
            break;
        default:
            throw prefix;
    }
}

void synthesizer::addTrk(u32 size){
    //ifile.ignore(size);
    //return;
    const size_t trkBegin = ifile.tellg();
    int trkNum=0;
    u8 code;
    std::string prefix;
    u64 cumulativeDelay=0;
    try{
        if((size_t)ifile.tellg() - trkBegin > size){
            throw std::runtime_error("Trk size incorrect");
        }
        while(true){
            prefix.clear();
            const u32 delta = getVLQ();
            cumulativeDelay+= delta;
            logger::miParserLog << "Delta: " << delta << std::endl;
            code = getUBE<u8>();
            prefix.push_back(code);
            switch(code){
                case 0xb0 ... 0xbf:
                    _parser_controlChange(code&0xf, cumulativeDelay);
                    break;
                case 0xc0 ... 0xcf:
                    _parser_programChange(code&0xf, cumulativeDelay);
                    break;
                case 0xff:
                    parseMetaEvent(prefix, trkNum, cumulativeDelay);
                    break;
                default:
                    throw prefix;
            }
        }
    }catch(const std::string & e){
        logger::miParserLog << "Unknown event prefix! (0x";
        for(const u8 &c: e){
            logger::miParserLog << std::hex << std::setw(2) << std::setfill('0') << (u32)c;
        }
        logger::miParserLog << ")" << std::endl;
        exit(0);
    }
    if((size_t)ifile.tellg() - trkBegin != size){
        throw std::runtime_error("Trk size incorrect");
    }
    //for(int i = 0; i < 4; ++i){
    //    u64 b = getUBE<u64>();
    //    std::cout << "0x" << std::hex << std::setw(16) << std::setfill('0') << b << std::endl;
    //}
}
