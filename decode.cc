#include"decode.h"
#include"log.h"
#include<iostream>
#include<fstream>
#include<string>
#include<cassert>
#include<cstring>

//RIFF uses little endian
template <typename T> T getLE(std::ifstream & ifile){
    T retVal=0;
    uint8_t buf[sizeof(T)];
    ifile.read(reinterpret_cast<char*>(buf), sizeof(T));
    for(auto i = 0u; i < sizeof(T); ++i){
        retVal+=static_cast<T>(buf[i])<<(8*i);
    }
    //if(retVal==1174405120){
    //    for(auto i = 0u; i < sizeof(buf)/sizeof(*buf);++i){
    //        logger::sfParserLog << i << ": " << std::hex << (uint32_t)buf[i] << std::endl;
    //    }
    //}
    return retVal;
}

soundfont::soundfont(std::string file_name) : fileName(file_name){}

void soundfont::open(){

    //char chunkHeaderID[sizeof(ckID)] = { 0 };
    uint32_t RIFF_length = 0;
    uint32_t RIFF_bytes_read = 4; //we assume te RIFF fccType is present
    uint32_t LIST_length = 0;
    uint32_t LIST_bytes_read = 0;

    logger::sfParserLog << "\nLoading soundfont \"" << fileName << "\"" << std::endl;
    ifile.open(fileName, std::ios::in | std::ios::binary);
    if(ifile.fail()){
        throw std::runtime_error(std::string("Failed to load soundfont file: ") + strerror(errno));
    }
    logger::sfParserLog << "Starting to parse soundfont" << std::endl;

    //read top-level RIFF chunk header
    {
        //check for RIFF tag
        if(getID<ckID>(ifile)!=ckID::RIFF){
            throw std::runtime_error("Expected RIFF at start of file");
        }
        //get RIFF length
        RIFF_length = getLE<uint32_t>(ifile);
        if(getID<fccType>(ifile)!=fccType::sfbk){
            //expecting sfbk RIFF type, this may not be a soundfont ...
            throw std::runtime_error("Expected RIFF chunk to have sfbk fccType");
        }
        logger::sfParserLog << "RIFF chunk has length: " << RIFF_length << "\n" << std::endl;
    }

    fccType listType = fccType::NONE;

    while(!ifile.eof()){
        //Each pass of this loop parses a single chunk
        //in the case of LIST or RIFF, it just parses the header
        uint32_t chunkSize = 0;
        uint32_t chunkPaddedSize = 0;

        ckID chunkType = getID<ckID>(ifile);
        chunkSize = getLE<uint32_t>(ifile);
        chunkPaddedSize = chunkSize + (chunkSize&1);
        if(listType!=fccType::NONE){
            LIST_bytes_read+=sizeof(ckID)+sizeof(fccType);
        }
        RIFF_bytes_read+=sizeof(ckID)+sizeof(fccType);
        //In an alternate universe, where RIFF rounds 
        //up chunk lengths to the nearest word instead
        //chunkPaddedSize = chunkSize&(~3) + (chunkSize&3?4:0);
        switch(chunkType){
            case ckID::RIFF: 
                throw std::runtime_error("Cannot nest RIFF chunks");
            case ckID::LIST:
                if(listType!=fccType::NONE){
                    throw std::runtime_error("Cannot nest LIST chunks");
                }
                listType = getID<fccType>(ifile);
                LIST_length = chunkPaddedSize;
                LIST_bytes_read = 0;
                RIFF_bytes_read+=sizeof(fccType);
            default:
                logger::sfParserLog << "Chunk type encountered: \"" << numToID(chunkType) << '\"' << std::endl;
                logger::sfParserLog << "\tSize: " << chunkSize << ", Skip: " << chunkPaddedSize << std::endl;
        }
        switch(chunkType){
            case ckID::LIST:
                logger::sfParserLog << "\tlistType: " << numToID(listType) << std::endl;
                break;
            default:
                ifile.ignore(chunkPaddedSize);
                break;
        }
        if(chunkType!=ckID::LIST){
            RIFF_bytes_read+=chunkPaddedSize;
        }
        logger::sfParserLog << "\tRIFF_bytes_read: " << RIFF_bytes_read << std::endl;
        if(listType!=fccType::NONE){
            LIST_bytes_read+=chunkPaddedSize;
        }
        if(RIFF_bytes_read>RIFF_length){
            throw std::runtime_error("File extends beyond RIFF chunk boundary");
        }
        if(listType!=fccType::NONE&&LIST_bytes_read>=LIST_length){
            if(LIST_bytes_read==LIST_length){
                LIST_bytes_read=0;
                listType=fccType::NONE;
            }else{
                throw std::runtime_error("Chunk boundary does not allign with LIST boundary");
            }
        }
    }
    if(RIFF_bytes_read<RIFF_length){
        logger::sfParserLog << "RIFF_bytes_read: " << RIFF_bytes_read << std::endl;
        logger::sfParserLog << "RIFF_length: " << RIFF_length << std::endl;
        throw std::runtime_error("Unexpected EOF in RIFF block");
    }
    if(listType!=fccType::NONE&&LIST_bytes_read<LIST_length){
        throw std::runtime_error("Unexpected EOF in LIST block");
    }
    logger::sfParserLog << "Finished parsing soundfont" << std::endl;
}
