#pragma once
#include<iostream>
#include<fstream>

namespace logger {
    extern bool initialized;
    extern std::ofstream sfParserLog;
    extern std::ofstream miParserLog;
    void init();
};
