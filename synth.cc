#include"synth.h"
#include"log.h"

int bpm = 144;
double timePerCrotchet = 60.0/bpm;

struct note{
    double pitch;
    double duration;
    double s;
    note(double _p, double _d, bool _s)
        : pitch(_p),
        duration(_d),
        s(_s) {}
};

struct _note{
    double pitch;
    double duration;
    struct {
        u16 c:1;
        u16 s:1;
        u16 loop_begin:1;
        u16 loop_end  :1;
    };
    _note(){
        pitch=duration=c=s=loop_begin=loop_end=0;
    }
    _note(int _p, int _d, bool _c=false, bool _s=false) 
        : pitch(261.6f*pow(2.0,(_p/12.0))), 
        duration(pow(2.0,_d)*1000),
        c(_c),
        s(_s),
        loop_begin(0),
        loop_end(0)
    {}
};

void setBPM(int _bpm){
    bpm=_bpm;
    timePerCrotchet = 60.0/bpm;
}

void puts16(FILE * output, s16 val){
    fwrite(&val, 1, 2, output);
}

_note cont(int dur, bool cont=false){
    return _note(0, dur, cont);
}

_note rest(int dur){
    return _note(0, dur, 0, 1);
}

_note loop_begin(){
    _note retVal;
    retVal.loop_begin = 1;
    return retVal;
}

_note loop_end(){
    _note retVal;
    retVal.loop_end = 1;
    return retVal;
}

std::vector<note> constructSong(const std::vector<_note> &input){
    std::vector<note> retVec;
    std::stack<std::pair<int,bool>> loop_stack;
    int last = 0;
    for(unsigned int i = 0u; i < input.size(); ++i){
        if(input[i].loop_begin){
            loop_stack.emplace(std::pair<int,bool>(i,true));
            continue;
        }
        if(input[i].loop_end){
            if(loop_stack.top().second){
                loop_stack.top().second=false;
                i=loop_stack.top().first;
            }else{
                loop_stack.pop();
            }
            continue;
        }
        if(last!=i&&input[last].c){
            retVec.back().duration+=input[i].duration;
        }else{
            retVec.push_back(
                note(
                    input[i].pitch, 
                    input[i].duration,
                    input[i].s));
        }
        last = i;
    }
    return retVec;
}

void play_song(FILE * output){
    std::vector<_note> song_input = {
#include"tetris.mus"
    };
    std::vector<note> song = constructSong(song_input);
    const int vol = 8192;
    const double rate = 44100;
    while(1){
        std::cout << "BPM: " << bpm << std::endl;
        double cur = 0.0f;
        for(auto &i : song){
            const double dur = (i.duration*timePerCrotchet*rate)/1000;
            const double pitch = i.pitch;
            if(i.s){
                for(int j = 0; j < dur; ++j){
                    puts16(output, 0);
                    puts16(output, 0);
                }
            }else{
                for(int j = 0; j < dur; ++j){
                    double gain = vol*(1-(j/dur));
                    cur = (double)j*2.0*(pitch/rate);
                    cur = gain * ((int)cur%2);
                    cur = (cur-gain/2)*2;
                    //cur = gain * sin(((double)j)*2.0*M_PI*(pitch/rate));
                    puts16(output, cur);
                    puts16(output, cur);
                }
            }
        }
        //setBPM(bpm+12);
    }
}

int main(int argc, char * argv[]){
    const std::string soundfontPath = "data/FluidR3_GM2-2.sf2";
    logger::init();
    synthesizer synth(soundfontPath);
    try{
        synth.load_soundfont();
    }catch(const std::runtime_error & e){
        std::cerr << "Error opening \"" << soundfontPath << "\": " << e.what() << std::endl;
        return -1;
    }
    auto audioCtx = popen("pacat --latency-msec=100 2>/dev/null >/dev/null", "w");
    //play_song(audioCtx);
    if(!audioCtx){
        std::cerr << "Failed to exec pacat" << std::endl;
        return 1;
    }
    if(argc < 2){
        std::cerr << "Please specify an input file" << std::endl;
        return 1;
    }
    for(int i = 1; i < argc; ++i){
        try{
            synth.load(argv[i]);
        }catch (const std::runtime_error & e){
            std::cerr << "Error parsing file \"" << argv[i] << "\": " << e.what() << std::endl;
        }
        try{
            synth.play_back(audioCtx);
        }catch (const std::runtime_error & e){
            std::cerr << "Error playing file \"" << argv[i] << "\": " << e.what() << std::endl;
        }
    }
    return 0;
}
