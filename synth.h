#pragma once
#include<iostream>
#include<iomanip>
#include<fstream>
#include<cstring>
#include<string>
#include<vector>
#include<cstdio>
#include<cmath>
#include<stack>
#include<utility>
#include"decode.h"
#include"log.h"

typedef   int8_t  s8;
typedef  uint8_t  u8;
typedef  int16_t s16;
typedef uint16_t u16;
typedef  int32_t s32;
typedef uint32_t u32;
typedef  int64_t s64;
typedef uint64_t u64;

class synthesizer {
    soundfont _soundfont;
    std::ifstream ifile;
    bool hasHeader;
    bool useSMPTE;
    uint16_t format;
    uint16_t nTrks;
    union {
        uint16_t ticksPerQuaterNote;
        struct {
            int8_t smpteFormat;
            int8_t ticksPerFrame;
        };
    };

    uint32_t getVLQ();
    template <typename T, size_t size=8*sizeof(T)> T getUBE();
    template <typename T, size_t size=8*sizeof(T)> T getULE();
    void unexpectedEOF();
    void addThd(uint32_t size);
    void addTrk(uint32_t size);

    void parseMetaEvent(std::string & prefix, int trkNum, u64 cumulativeDelay);

    void _parser_getSeqName(int trkNum);
    void _parser_getCopyRight(int trkNum);
    void _parser_getTextEvent(int trkNum);
    void _parser_getTextMarker(int trkNum);
    void _parser_setTimeSignature();
    void _parser_setKeySignature();
    void _parser_setTempo();
    void _parser_programChange(u8 channelNo, u64 cumulativeDelay);
    void _parser_controlChange(u8 channelNo, u64 cumulativeDelay);

    struct note{
        double delta;
        double pitch;
        double duration;
        bool silent;
        note(double _p, double _d, bool _s)
            : pitch(_p),
            duration(_d),
            silent(_s) {}
    };
    struct track{
        std::vector<note> song;
    };
    std::vector<track> tracks;
public:
    synthesizer(std::string soundfont) 
        : _soundfont(soundfont),
        hasHeader(false) {}
    ~synthesizer(){}
    void load_soundfont(){
        _soundfont.open();
    }
    void load(std::string fileName);
    void play_back(const FILE * ctx) const {
        if(!hasHeader){
            throw std::runtime_error("No file loaded");
        }
    }
};
