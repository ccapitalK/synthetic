CXX=g++
CXXFLAGS=-Wall -Wextra -std=gnu++17
OBJS=synth.o decode.o midi.o log.o

synth: $(OBJS)
	$(CXX) $(CXXFLAGS) $^ -o $@

clean: 
	rm *.o synth

%.o: %.cc
	$(CXX) $(CXXFLAGS) $^ -o $@ -c
